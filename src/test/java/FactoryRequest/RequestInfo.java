package FactoryRequest;

public class RequestInfo {

    String URL;
    String body;

    public  RequestInfo() {

    }
    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }


}
