package util;

public class ApiConfiguration {
    public static final String CREATE_ITEM = GetProperties.getInstance().getHost()+"/api/items/11065454.json";
    public static final String READ_ITEM = GetProperties.getInstance().getHost()+"/api/items/%s.json";
    public static final String UPDATE_ITEM = GetProperties.getInstance().getHost()+"/api/items/%s.json";
    public static final String DELETE_ITEM = GetProperties.getInstance().getHost()+"/api/items/%s.json";
}
